export function filterEntries(inputData, formRef, filters) {
	const formArray = Array.from(formRef).filter((el) => {
		return el.type !== 'submit'
	})

	return formArray.reduce((total, el) => {
		if (!el.value) return total

		return filters
			.map(({ fieldtype, condition }) => {
				if (el.dataset.type === fieldtype) {
					return total.filter((x) => condition(x, el))
				}
			})
			.filter((e) => e && e.length)
			.flat()
	}, inputData)
}

export const sortEntries = {
	ascending: (inputData, key) => {
		if (!inputData.length) return []

		// Deep copies array before sorting to prevent mutation
		return JSON.parse(JSON.stringify(inputData)).sort((p, n) =>
			p[key] > n[key] ? 1 : -1
		)
	},

	descending: (inputData, key) => {
		if (!inputData.length) return []

		// Deep copies array before sorting to prevent mutation
		return JSON.parse(JSON.stringify(inputData)).sort((p, n) =>
			p[key] < n[key] ? 1 : -1
		)
	},

	alternate: function (inputData, key) {
		if (!inputData.length) return []

		// Checks if data is already sorted in ascending order
		function isAscending() {
			for (let i = 0; i < inputData.length - 1; i++) {
				if (inputData[i][key] > inputData[i + 1][key]) {
					return false
				}
			}
			return true
		}

		return isAscending()
			? this.descending(inputData, key)
			: this.ascending(inputData, key)
	}
}

export function paginateEntries(inputData, givenPage, perPage) {
	const pagesTotal = Math.ceil(inputData.length / perPage)

	let currentPage = 1

	if (givenPage <= pagesTotal && givenPage > 0) {
		currentPage = givenPage
	}

	const paginationStart = perPage * (currentPage - 1)
	const paginationEnd = perPage * currentPage
	const paginatedEntries = inputData.slice(paginationStart, paginationEnd)

	return {
		pagesTotal,
		currentPage,
		paginatedEntries
	}
}
